//Aharon Moryoussef
//1732787

package javaFx_lab6;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame rps = new RpsGame();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		
		Button rock = new Button ("rock");
		Button paper = new Button("paper");
		Button scissors = new Button("scissors");
		
		buttons.getChildren().addAll(rock,paper,scissors);
		TextField welcome = new TextField("Welcome");
		welcome.setPrefWidth(200);
		TextField win = new TextField("Wins: " + rps.getWin());
		TextField losses = new TextField("Losses: " + rps.getLosses());
		TextField tie = new TextField("Tie: " + rps.getTies());
		textFields.getChildren().addAll(welcome,win,losses,tie);
		
		
		RpsChoice rpsRock = new RpsChoice(welcome,win,losses,tie,"rock",rps);
		RpsChoice rpsPaper = new RpsChoice(welcome,win,losses,tie,"paper",rps);
		RpsChoice rpsScissors = new RpsChoice(welcome,win,losses,tie,"scissors",rps);
		
		rock.setOnAction(rpsRock);
		paper.setOnAction(rpsPaper);
		scissors.setOnAction(rpsScissors);
		
		overall.getChildren().addAll(buttons,textFields);
		root.getChildren().addAll(overall);
		
		
		stage.show(); 
		
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

