//Aharon Moryoussef
//1732787
package javaFx_lab6;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField msg;
	private TextField w;
	private TextField l;
	private TextField t;
	private String choice;
	private RpsGame n = new RpsGame();
	

	
	public RpsChoice (TextField message,TextField wins,
			TextField losses, TextField ties, 
			String playerChoice,RpsGame n)
	{
		this.msg = message;
		this.w = wins;
		this.l = losses;
		this.t = ties;
		this.choice = playerChoice;
		this.n = n;
		
		
		
	}

	@Override
	public void handle(ActionEvent e) {
		String outcome = this.n.playRound(this.choice);
		this.msg.setText(outcome);
	    this.w.setText("Wins: " + n.getWin());
	    this.l.setText("Losses: " + n.getLosses());
	    this.t.setText("Tie: " + n.getTies());
    
	 
	}

}
