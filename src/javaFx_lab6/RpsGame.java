//Aharon Moryoussef
//1732787
package javaFx_lab6;

import java.util.Random;



public class RpsGame {
	private int win=0;
	private int losses=0;
	private int ties=0;
	private Random generator = new Random(3);


	public int getWin() {
		return this.win;
	}
	public int getLosses() {
		return this.losses;
	}
	public int getTies() {
		return this.ties;
	}
	
	public String playRound(String playersChoice) {
		int computerChoice = generator.nextInt(3);
		String outcome= "";
		//IF player chooses rock
		if(playersChoice.toLowerCase().equals("rock") &&
				computerChoice == 0) {
			this.ties++;
			outcome = "TieBreaker!" + "Computer picked Rock!" ;
		}else if(playersChoice.toLowerCase().equals("rock") && 
				computerChoice == 1) {
			this.losses++;
			outcome = "You Lose!" + "Computer picked Paper!"; 
		}else if(playersChoice.toLowerCase().equals("rock") && 
				computerChoice == 2) {
			this.win++;
			outcome = "You Win! " + "Computer picked scissors";
			//IF player chooses paper
		}else if(playersChoice.toLowerCase().equals("paper") &&
				computerChoice == 0) {
			this.win++;
			outcome = "You Win! " + "Computer picked Rock";
		}else if(playersChoice.toLowerCase().equals("paper") && 
				computerChoice == 1) {
			this.ties++;
			outcome = "TieBreaker! " + "Computer picked Paper"; 
		}else if(playersChoice.toLowerCase().equals("paper") && 
				computerChoice == 2) {
			this.losses++;
			outcome = "You Lose! " + " Computer picked Scissors";
			//IF player chooses scissors
		}else if(playersChoice.toLowerCase().equals("scissors") &&
				computerChoice == 0) {
			this.losses++;
			outcome = "You Lose! " + "Computer picked Rock!";
		}else if(playersChoice.toLowerCase().equals("scissors") && 
				computerChoice == 1) {
			this.win++;
			outcome = "You Win! " + "Computer picked Paper!";
		}else if(playersChoice.toLowerCase().equals("scissors") && 
				computerChoice == 2) {
			this.ties++;
			outcome = "TieBreaker! " + "Computer picked Scissors!";
		}
		return outcome;
	}
}


